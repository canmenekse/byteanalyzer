'''
Created on Feb 13, 2016

@author: Balaji
'''

import binascii
import os
import struct


headerFP = None
trailerFP = None
byteArraySize = 16
path = ""


def createTrailerProfile(trailerSize,fileContent):
    trailerArray = [[0]*256 for i in range(trailerSize)]
    index = 0
    fileSize = len(fileContent)
    while index < trailerSize :
        if(fileSize-1 < index):
            trailerArray[index] = [-1]*256
        else:
            trailerArray[index][fileContent[fileSize-index-1]] = 1
        index = index + 1
    return trailerArray
        

def createHeaderProfile(headerSize,fileContent):
    headerArray = [[0]*256 for i in range(headerSize)]
    index = 0
    while index < headerSize:
        if(len(fileContent) -1 < index):
            headerArray[index] = [-1]*256
        else:
            #print(fileContent[index])
            headerArray[index][fileContent[index]] = 1
        index = index + 1
    return headerArray

def computeFingerPrint(byteArray,fingerPrint,prevCount):
    global byteArraySize
    for bytePos in range(len(byteArray)):
        for byteValue in range(len(byteArray[bytePos])):
            if not fingerPrint:
                fingerPrint = [[0]*256 for i in range(byteArraySize)]
            fingerPrint[bytePos][byteValue] = ((fingerPrint[bytePos][byteValue] * prevCount) + byteArray[bytePos][byteValue])/(prevCount + 1)
    return fingerPrint
    

def readFileBytes(filename,fileCount):
    f = open(filename,"rb")
    global byteArraySize
    global headerFP
    global trailerFP
    bytesArray = [0] * 256
    index = 0
    fileContent = f.read()
    f.close()
    headerArray = createHeaderProfile(byteArraySize, fileContent)
    headerFP = computeFingerPrint(headerArray,headerFP,fileCount)
    trailerArray = createTrailerProfile(byteArraySize,fileContent)
    trailerFP = computeFingerPrint(trailerArray, trailerFP, fileCount)
    
    
def generateFHT_from_dir(srcDir,byte_array_size,percent):
    global byteArraySize
    global headerFP
    global trailerFP
    headerFP=None
    trailerFP=None
    path = srcDir;
    files = os.listdir(path)
    end = len(files) * percent / 100
    byteArraySize=byte_array_size
    fileCount = 0
    for file in files:
        if(file.startswith(".")):
            continue
        readFileBytes(os.path.join(path,file),fileCount)
        fileCount += 1
        if fileCount >=end:
            break
        print("FHT Analyzed " + str(fileCount) + " file " + str(byteArraySize))
    return headerFP,trailerFP

def generateFHT_from_file(srcFile,byte_array_size):
    global byteArraySize
    global headerFP
    global trailerFP
    headerFP=None
    trailerFP=None
    byteArraySize=byte_array_size
    readFileBytes(os.path.join(path,srcFile),1)
    return headerFP,trailerFP



