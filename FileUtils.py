__author__ = 'canmenekse'
import os, datetime
import config

#Sets the directory for the analysis
def set_new_analysis_dir(destination=os.getcwd()):
    #it creates a new directory for each run , the info is stored in there
    new_directory = os.path.join(destination, datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
    try:
        os.mkdir(new_directory)
    except:
        print(" ERROR: Directory could not be created")

    config.cur_analysis_dir = new_directory;

#Creates a new directory with the given name
def generate_dir_with_mime_type(mime_type):
    new_dir_with_mime_type=os.path.join(config.cur_analysis_dir,mime_type)
    try:
        os.mkdir(new_dir_with_mime_type)
    except:
        print(" ERROR:  Directory could not be created")
    return new_dir_with_mime_type

def get_main_analysis_dir():
    return config.cur_analysis_dir


def get_analysis_dir_with_mime_type(mime_type):
    return os.path.join(config.cur_analysis_dir,mime_type)