
import config
import math
import ByteReader
import ArrayUtils

#Does correlation analysis between two files
def correlation_analysis_files(filePath1,filePath2):
    #Read the bytes from files first
    file1_byte_array=ArrayUtils.normalize_array(ByteReader.get_byte_frequency_from_file(filePath1))
    file2_byte_array=ArrayUtils.normalize_array(ByteReader.get_byte_frequency_from_file(filePath2))
    #Do the analysis
    return correlation_analysis_arrays(file1_byte_array,file2_byte_array)

def correlation_analysis_arrays(array1,array2):
    correlation = [0]*config.byte_array_size
    for i in range(0,config.byte_array_size):
        diff = abs(float(array1[i]) - float(array2[i]))
        if diff>=config.BFC_correlation_treshold:
            correlation[i]=0
        else:
            correlation[i]=1
    return correlation




