__author__ = 'canmenekse'
#Useful to reuse created fingerprints
def read_fprint_from_file(filename):
    first_line=""
    analyzed_file_count=""
    with open(filename,'r') as file:
        #array info is stored in the first line
        array = file.readline().strip().split(",")
        analyzed_file_count=int(file.readline().strip())
    for i in range(0,len(array)):
        array[i]= float(array[i])
    return array,analyzed_file_count
