import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.mime.MediaType;
import org.codehaus.plexus.util.FileUtils;

public class Main {
	public static String destinationPath = "D:\\output";
	public static HashMap<String, Long> foundFileTypes;
	public static Tika tika;
	public static boolean doNotRecreateDirectories = true;
	public static int fileCount = 0;
	public static int mimeTypesCount = 0;

	public static void main(String[] args) throws IOException,
			UnsupportedEncodingException {
		tika = new Tika();
		foundFileTypes = new HashMap<String, Long>();
		// TODO Auto-generated method stub
		String rootPath ="D:\\extra";
		if (doNotRecreateDirectories) {
			insertExistingDirectories();
		}
		traverse(rootPath);
		System.out.println("Copied " + fileCount + " files");
		System.out.println("Tika found " + mimeTypesCount + " MIME types");
		System.out.println("Found MIMETypes:");

		for (Entry<String, Long> entry : foundFileTypes.entrySet()) {
			String key = entry.getKey();
			System.out.println(key + " : " + entry.getValue().toString());
		}
		printToFileAsJson();

	}

	public static void printToFileAsJson() {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("filetypes.json");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.println("{");
		writer.println("\"types\": [");
		
		for (Entry<String, Long> entry : foundFileTypes.entrySet()) {
			String key = entry.getKey();
			writer.println("{\"type\": \""+key + "\", \"value\": " + entry.getValue().toString()+"},");
			
			
		}
		writer.println("]");
		writer.println("}");
		writer.close();
	}

	public static void insertExistingDirectories() {
		File destinationFolder = new File(destinationPath);
		File[] files = destinationFolder.listFiles();
		for (File f : files) {

			if (f.isDirectory()) {
				String normalizedType = f.getName();
				foundFileTypes.put(normalizedType, (long) 0);
			}
		}

	}

	public static String getFileType(String path) {
		try {
			return tika.detect((new File(path)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ERROR";
	}

	public static String getCopyPath(String directoryName) {
		directoryName = convertToValidDirName(directoryName);
		return destinationPath + "\\" + directoryName;
	}

	public static void createDirectory(String path) {
		File f = new File(path);
		try {
			if (f.mkdir()) {
				// System.out.println("Directory Created");
			} else {
				// System.out.println("Directory is not created");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String convertToValidDirName(String directoryName) {
		String converted = directoryName.replace('/', '_');
		return converted;
	}

	public static void copyFile(String source, String destination,
			String filename) {
		File src = new File(source);
		File dest = new File(destination + "\\" + filename);
		try {
			FileUtils.copyFile(src, dest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void traverse(String root) {
		try {

			File rootFolder = new File(root);
			File[] files = rootFolder.listFiles();

			for (File f : files) {

				if (f.isDirectory()) {
					traverse(f.getAbsolutePath());
				}
				// It is a file
				else {
					fileCount++;
					String type = getFileType(f.getAbsolutePath());
					String normalizedType = convertToValidDirName(type);
					String src = f.getAbsolutePath();
					String destination = getCopyPath(type);
					String fileName = f.getName();
					if (foundFileTypes.containsKey(normalizedType)) {
						getCopyPath(type);
						Long valueOfTheFoundType = foundFileTypes
								.get(normalizedType);
						foundFileTypes.put(normalizedType,
								valueOfTheFoundType + 1);
					}
					// We found new MIME type!
					else {
						mimeTypesCount++;
						createDirectory(getCopyPath(type));
						// Now it is founded
						foundFileTypes.put(normalizedType, (long) 1);
					}

					copyFile(src, destination, fileName);
					System.out.println("Copying " + src + " to " + destination);
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
