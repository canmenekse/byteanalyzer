#Please put this file in the same directory as other files(basically one directory up) for it to import necessary things

import FHT
import Export
import os
print(os.getcwd())
#Set the directories for the files
d3_vis_dir = os.path.join(os.getcwd(),"vis")
fhd_vis_header_file = os.path.join(d3_vis_dir,"FHT_H.html")
fhd_vis_trailer_file = os.path.join(d3_vis_dir,"FHT_T.html")
byte_array_size = 16;
header,trailer = FHT.generateFHT_from_file("samplefile1",byte_array_size)
Export.export_FHT_analysis_from_array(fhd_vis_header_file,fhd_vis_trailer_file,header,trailer,byte_array_size,"Header_FHT","Trailer_FHT")

