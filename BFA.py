__author__ = 'canmenekse'
import ArrayUtils
import ByteReader
import os
import config


#Generates fingerprint from a single directory with given percentage of files to analyze
def generate_fprint_from_dir(root,percent):
    print("Finding the size of the files")
    total_file_count = len(os.listdir(root))
    print("Size is: " + str(total_file_count) )
    num_of_files_to_process = int(total_file_count*(percent/100))
    print("Will process: " + str(num_of_files_to_process) )
    return generate_fprint(root,num_of_files_to_process), num_of_files_to_process


#Actually generates the fingerprint for the files in the document
def generate_fprint(root,num_of_files_to_process):
    fprint= config.byte_array_size * [0.0]
    files_in_dir=[]
    processed=0
    print("Gettting the files from " + root)
    files_in_dir=os.listdir(root)
    files_in_dir.sort()
    for item in files_in_dir:
        fullPath =os.path.join(root,item)
        if os.path.isfile(fullPath):
            if processed<num_of_files_to_process:
                processed+=1
                process_file_for_fingerprint(fullPath,fprint,processed)
                print("BFA Processed " +str(processed) +"/" + str(num_of_files_to_process))
                files_in_dir.append(fullPath)
    return fprint


#To make it all in one pass it modifies the existing fingerprint , normalization is done at the end.
def modify_fprint_byte(array,fprint,processed_so_far):

    max = ArrayUtils.find_max(array)
    for i in range (0,len(array)):
        if max !=0:
            array[i]=array[i]/max
        else:
            array[i]=0;
        #modify the byte of the fingerprint
        fprint[i] =(( fprint[i] * processed_so_far) + array[i])/(processed_so_far+1)


#Wrapper for processing a single file
def process_file_for_fingerprint(filename,fprint,processed_so_far):
    array = ByteReader.get_byte_frequency_from_file(filename)
    modify_fprint_byte(array,fprint,processed_so_far);