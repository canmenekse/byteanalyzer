import os
import Export
import ByteReader
import BFC_Cross_Correlation
import ArrayUtils
#Set the directories for the files
d3_vis_dir = os.path.join(os.getcwd(),"vis")
matrix_vis_file = os.path.join(d3_vis_dir,"matrix.html")
fprint = ByteReader.get_byte_frequency_from_file("samplefile1")
fprint = ArrayUtils.normalize_array(fprint)
cross_correlation_matrix = BFC_Cross_Correlation.generate_cross_corelation(fprint,1)
Export.export_cross_correlation_analysis_from_matrix(matrix_vis_file,"BFC_Cross_Correlation",cross_correlation_matrix,1)
