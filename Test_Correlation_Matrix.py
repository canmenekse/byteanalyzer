__author__ = 'canmenekse'
import BFC
import BFA
import config
import unittest
import os
import ArrayUtils
import sys




class Test_BFC_Matrix(unittest.TestCase):
    def test_simple_equal_frequency(self):
        test_folder=os.path.join(os.getcwd(),"Cases","Simple")
        fprint,last_processed = BFA.generate_fprint_from_dir(test_folder,100)
        fprint=ArrayUtils.normalize_array(fprint)
        m = BFC.generate_cross_corelation(fprint,last_processed)
        if(m[97][98]!=0.0):
            assert False
        else:
            for i in range (0,config.byte_array_size):
                if m[97][i] != 1.0 and (i!= 97 and i!=98):
                    print("The issue is with " + str(m[97][i]))
                    assert False
            assert True
    def test_normalization_simple_equal_frequency(self):
        test_folder=os.path.join(os.getcwd(),"Cases","Simple")
        fprint,last_processed = BFA.generate_fprint_from_dir(test_folder,100)
        fprint=ArrayUtils.normalize_array(fprint)
        m = BFC.generate_cross_corelation(fprint,last_processed)
        if(m[97][98]!=0.0):
            assert False
        else:
            for i in range (0,config.byte_array_size):
                if m[97][i] != 1.0 and (i!= 97 and i!=98):
                    print("The issue is with " + str(m[97][i]))
                    assert False
            assert True





if __name__ == '__main__':
    unittest.main()