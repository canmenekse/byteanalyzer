__author__ = 'canmenekse'
import struct
import config
#Constructs an array from the bytes in file according to frequencies
def get_byte_frequency_from_file(filename):
    #Ascii Array
    array = config.byte_array_size * [0.0]
    with open(filename,"rb") as file:
        byteRead = file.read(1)
        while byteRead :
            byteValue = (struct.unpack("B",byteRead))[0];
            byteRead = file.read(1)
            if byteValue<config.byte_array_size:
                array[byteValue]+=1
    return array