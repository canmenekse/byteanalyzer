# README #

### How do I get set up? ###
1. Download the python files in this repo. Change the path_combination = "\\" to path_combination="/" when using OSX
2. Put the folder called "original" in the same folder as the python scripts
3. create an empty folder called "analysis" in the same folder with the python scripts. Your analysis will be stored on this folder.
4. In "Main.py" modify root with the directory you want to perform analysis on.
as in root = "D:\\Debug"