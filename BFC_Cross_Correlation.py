__author__ = 'canmenekse'
import config
#Generates cross correlation matrix, it stores the non normalized frequencies in left side of the diagonal and the normalized ones
#at the right side of the diagonal. It returns the generated Matrix.
def generate_cross_corelation(array,number_of_files):
    max=-2
    correlation_matrix = [ [ 0 for i in range(config.byte_array_size) ] for j in range(config.byte_array_size) ]
    threshold = config.BFC_matrix_display_threshold;
    for i in range(0,len(array)):
        for j in range(0,len(array)):
            if i > j:
                if  not (array[i] == 0.0 and array[j] == 0.0):
                    if  array[i]> threshold or array[j]>threshold:
                        value = round(float(1-abs(array[i]-array[j])),2)
                        if value>max:
                            max = value
                        correlation_matrix[i][j] = round(value,2)
                    else:
                        correlation_matrix[i][j] = "N.A"
                else:
                    #print("Put a N.A")
                    correlation_matrix[i][j] = "N.A"
            elif i==j:
                    correlation_matrix[i][j] = number_of_files
    for i in range(0,len(array)):
        for j in range(0,len(array)):
            if i < j:
                if  not (correlation_matrix[j][i]=="N.A"):
                    correlation_matrix[i][j] = round(correlation_matrix[j][i]/max,2)
                else:
                    correlation_matrix[i][j] = "N.A"

    return correlation_matrix