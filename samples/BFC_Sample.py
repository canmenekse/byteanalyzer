#Please put this file in the same directory as other files(basically one directory up) for it to import necessary things

import os
import config
import Export
import FileUtils

#Set the directories for the files
d3_vis_dir = os.path.join(os.getcwd(),"vis")
corr_vis_file = os.path.join(d3_vis_dir,"corr.html")
Export.export_correlation_analysis_from_files(corr_vis_file,"BFC","samplefile1","samplefile2")
