import math
#Applies the aLaw Companding function to an element
def aLaw(element):
	A = 87.6
	B = 1/A
	if element < B:
		new_element = (A*element)/(1+math.log(A))
	else:
		new_element = (1+math.log(A*element))/(1+math.log(A))
	return new_element


#Finds the maximum element in the array
def find_max(array):
    max = -1;
    for i in range(0,len(array)):
        if array[i]>=max:
            max = array[i]
    return max


#Divides the elements in the array with the max number in array and rounds the number
def normalize_array(array):
    max = find_max(array)
    for i in range(0,len(array)):
        if max !=0:
            array[i] = round(array[i]/max,2)
        else:
            array[i]=0
    return array


#Applies the companding function to an array and returns the applied version of it
def apply_companding_function(array):
    maximum = max(array)
    new_array=[]
    for i in range(0,len(array)):
        new_array.append( round(aLaw(float(array[i])/float(maximum)),4))
    return new_array