__author__ = 'canmenekse'
import os
import config
import BFA
import BFC_Cross_Correlation
import ByteReader
import ArrayUtils
import Export
import FileUtils
import FHT

#Given a root this program runs BFA,BFC,BFC Cross Correlation and FHT on the files in the directories however
# it does not apply it if the files have higher depth than 1 , root/K and root/L are traversed but if there is root/K/P it is ignored
# This does make sense when traversing file types classified by Tika
def run():
    print("Program Started")
    #To check the execution duration of the program
    root="D:\\mimetypesCheckCompanding"
    #root ="/Users/canmenekse/Documents/debug/types"

    #This overrides the default ammount of percentages
    file_types_percent ={"application_octet-stream":75}
    all_items_in_dir = os.listdir(root)
    all_items_in_dir.sort()
    #Set the directories for the files

    analysis_dir = config.analysis_path
    d3_vis_dir = os.path.join(os.getcwd(),"vis")
    matrix_vis_file = os.path.join(d3_vis_dir,"matrix.html")
    correlation_vis_file = os.path.join(d3_vis_dir,"corr.html")
    bfa_vis_file = os.path.join(d3_vis_dir,"bfa.html")
    fhd_vis_header_file = os.path.join(d3_vis_dir,"FHT_H.html")
    fhd_vis_trailer_file = os.path.join(d3_vis_dir,"FHT_T.html")
    #How many bytes to read in FHT
    fhd_byte_array_size = 16

    FileUtils.set_new_analysis_dir(analysis_dir)
    for item in all_items_in_dir :
        #How much of the files in a folder will be processed
        percent =75;
        full_path =os.path.join(root,item)
        print("CHECKING DIR " + full_path)
        if os.path.isdir(full_path):
            if item in file_types_percent:
                percent = file_types_percent[item]
            mime_type =item
            mime_dir=FileUtils.generate_dir_with_mime_type(mime_type)
            fprint,analyzed_file_count = BFA.generate_fprint_from_dir(full_path,percent)
            #Normalizing the array we could also additionally use  Companding function if we wanted
            fprint = ArrayUtils.normalize_array(fprint)
            #Create the charts of BFA
            Export.export_BFA_analysis_from_array(bfa_vis_file,fprint,mime_type+"_BFA_",analyzed_file_count,mime_dir)
            header,trailer=FHT.generateFHT_from_dir(full_path,fhd_byte_array_size,percent)
            Export.export_FHT_analysis_from_array(fhd_vis_header_file,fhd_vis_trailer_file,header,trailer,fhd_byte_array_size,str(fhd_byte_array_size)+"_FHTheader_",
                                                  str(fhd_byte_array_size)+"_FHTtrailer_",mime_dir)

           #Generate correlation Matrix from the fprint
            print("GENERATING CORRELATION MATRIX")
            cross_correlation_matrix = BFC_Cross_Correlation.generate_cross_corelation(fprint,analyzed_file_count)
            print("GENERATED CORRELATION MATRIX")
            Export.export_cross_correlation_analysis_from_matrix(matrix_vis_file,mime_type+"_matrix_",cross_correlation_matrix,analyzed_file_count,mime_dir)
            print("Getting rest of the files from " + full_path + " for correlation analysis")
            files =os.listdir(full_path)
            files.sort()
            ##Correlation Analysis
            length_of_remaining = len(files) - analyzed_file_count
            correlation_processed = 0 ;
            ##Use the rest of the files for correlation analysis
            for i in range(analyzed_file_count,len(files)):
              correlation_processed +=1
              full_path = os.path.join(root,item,files[i])
              array = ArrayUtils.normalize_array(ByteReader.get_byte_frequency_from_file(full_path))
              # array = ArrayUtils.apply_companding_function(array)
              Export.export_correlation_analysis_from_arrays(correlation_vis_file,mime_type+"_corr_"+files[i]+"_",fprint,array,mime_dir)
              print("Correlation Analyis Done " + str(correlation_processed) + "/ " + str(length_of_remaining) )

run()







