#Please put this file in the same directory as other files(basically one directory up) for it to import necessary things
import os
import Export


#Set the directories for the files
d3_vis_dir = os.path.join(os.getcwd(),"vis")
bfa_vis_file = os.path.join(d3_vis_dir,"bfa.html")
Export.export_BFA_analysis_from_file(bfa_vis_file,"samplefile1","BFA_analysis")