__author__ = 'canmenekse'
import config
import os;
import FileUtils
import ByteReader
import ArrayUtils

####Public methods#####
#Method that creates files necessary for visualizing the matrix
def export_cross_correlation_analysis_from_matrix(d3_file_path,output_filename,matrix,analyzed_file_count,destination=os.getcwd()):
    _export_matrix_json(matrix,output_filename,destination)
    _export_chart(d3_file_path,output_filename,analyzed_file_count,destination)


#Method that creates files necessary for visualizing BFA from file
def export_BFA_analysis_from_file(d3_file_path,file_path,output_name,destination=os.getcwd()):
    byte_array =ArrayUtils.normalize_array(ByteReader.get_byte_frequency_from_file(file_path))
    _export_array_json(byte_array,output_name,destination)
    _export_chart(d3_file_path,output_name,1,destination)


#Method that creates files necessary for visualizing BFA from array
def export_BFA_analysis_from_array(d3_file_path,byte_array,output_name,analyzed_file_count=1,destination=os.getcwd()):
    _export_array_json(byte_array,output_name,destination)
    _export_chart(d3_file_path,output_name,analyzed_file_count,destination)


#Method that creates files necessary for visualizing FHT from header and trailer array
def  export_FHT_analysis_from_array(d3_header_path,d3_trailer_path,header,trailer,byte_size,header_output_name,trailer_output_name,destination=os.getcwd()):
    _export_FHT_files(header,trailer,destination,header_output_name,trailer_output_name)
    _export_FHT_chart(d3_header_path,header_output_name,destination)
    _export_FHT_chart(d3_trailer_path,trailer_output_name,destination)

#Method that creates files necessary for visualizing Correlation analysis between two arrays

def export_correlation_analysis_from_arrays(d3_file_path,output_filename,array1,array2,destination=os.getcwd()):
    _export_correlation_tsv(array1,array2,output_filename,destination)
    _export_correlation_chart(d3_file_path,output_filename,destination)

#Method that creates files necessary for visualizing Correlation analysis between two files
def export_correlation_analysis_from_files(d3_file_path,output_filename,file1_path,file2_path,destination=os.getcwd()):
    array1=ArrayUtils.normalize_array(ByteReader.get_byte_frequency_from_file(file1_path))
    array2=ArrayUtils.normalize_array(ByteReader.get_byte_frequency_from_file(file2_path))
    _export_correlation_tsv(array1,array2,output_filename,destination)
    _export_correlation_chart(d3_file_path,output_filename,destination)

##Private methods

#This method is useful when used for exporting a fingerprint for later use
def _export_BFA_txt(byte_array,output_name,analyzed_file_count,destination=os.getcwd()):
    s =""
    for i in range(0,len(byte_array)):
       s+=str(round(byte_array[i],2))
       if(i!=len(byte_array)-1):
            s+=","
    s+="\n" + str(analyzed_file_count)
    with open(os.path.join(destination,output_name+".txt"), "w") as text_file:
        text_file.write("");
        text_file.write(s)
#Used for creating a json representation of BFA


def _export_array_json(byte_array,output_name,destination=os.getcwd()):
    json ='{"data":['
    for i in range(0,len(byte_array)):
        json+='{"c":' + '"'+str(i)+'" ,'
        json+='"freq":' + str(round(byte_array[i],2))+"}"
        if(i!=len(byte_array)-1):
            json+=","

    json+="]}"
    with open(os.path.join(destination,output_name+".json"), "w") as text_file:
        text_file.write("");
        text_file.write(json)


#Export chart is used for multiple analysis it just replaces the filename with the given filename and
#puts the number of analyzed files
def _export_chart(d3_file_path,output_filename,analyzed_item_count,destination=os.getcwd()):
    file_path = os.path.join(os.getcwd(),d3_file_path)
    with open (file_path,'r') as file:
        content = file.read()
    data_file = output_filename+".json"
    new_html_file =os.path.join(destination,output_filename+".html")
    content=content.replace("filetypes.json",data_file)
    content = content.replace("XNUM Files",str(analyzed_item_count)+" Files")
    with open (new_html_file,"w") as html_file:
        html_file.write("")
        html_file.write(content)


#Similar thing with the export chart that is specific to the correlation chart
def _export_correlation_chart(d3_file_path,output_filename,destination=os.getcwd()):
    file_path = os.path.join(os.getcwd(),d3_file_path)
    with open (file_path,'r') as file:
        content = file.read()
    data_file = output_filename+".tsv"
    new_html_file = os.path.join(destination,output_filename+".html")
    content=content.replace("filetypes.tsv",data_file)
    with open (new_html_file,"w") as html_file:
        html_file.write("")
        html_file.write(content)


#Similar thing with the export chart that is specific to the FHT chart
def _export_FHT_chart(d3_file_path,output_filename,destination=os.getcwd()):
    file_path = os.path.join(os.getcwd(),d3_file_path)
    with open (file_path,'r') as file:
        content = file.read()
    data_file = output_filename+".csv"
    new_html_file = os.path.join(destination,output_filename+".html")
    content=content.replace("filetypes.csv",data_file)
    with open (new_html_file,"w") as html_file:
        html_file.write("")
        html_file.write(content)


#Generates the files to store data needed for visualizations of FHT
def _export_FHT_files(headerFP,trailerFP,destDir,header_output_name,trailer_output_name):
    hf_name = os.path.join(destDir,header_output_name)
    hf = open(hf_name+".csv","w")
    for i in range(256):
        hf.write(",Byte "+str(i))
    hf.write("\n")
    for bytePos in range(len(headerFP)):
        hf.write("Header "+str(bytePos))
        for byteValue in range(len(headerFP[bytePos])):
            hf.write(","+str(headerFP[bytePos][byteValue]))
        hf.write("\n")
    #hf.write(str(headerFP))
    hf.close()
    tf_name = os.path.join(destDir,trailer_output_name)
    tf = open(tf_name+".csv","w")
    for i in range(256):
        tf.write(",Byte "+str(i))
    tf.write("\n")
    for bytePos in range(len(trailerFP)):
        tf.write("Trailer "+str(bytePos))
        for byteValue in range(len(trailerFP[bytePos])):
            tf.write(","+str(trailerFP[bytePos][byteValue]))
        tf.write("\n")
    tf.close()



#Generates the files to store data needed for visualizations of Correlation analysis
def _export_correlation_tsv(array1,array2,output_filename,destination=os.getcwd()):
    new_file = os.path.join(destination,output_filename+".tsv")
    with open(new_file,'w') as file:
        file.write("bit\tcorr1\tcorr2\n")
        for i in range(0,config.byte_array_size):
             file.write(str(i)+"\t"+str(array1[i])+"\t"+str(array2[i])+"\n")
    return os.path.basename(new_file)


#Generates the files to store data needed for visualizations of BFC correlation Matrix
def _export_matrix_json(matrix,output_name,destination):
    json ='{"data":['
    for i in range(0,len(matrix)):
        for j in range (0,len(matrix[i])):
            if(i>j and matrix[i][j]!="N.A" and matrix[i][j]>config.BFC_matrix_correlation_threshold):
                json+='{"s":' + '"'+str(i)+'" ,'
                json+='"t":"' + str(j)+'",'
                json+='"v":"' + str(round(matrix[i][j],2))+'"}'
                json+=","
    json = json[:-1]
    json+="]}"

    with open(os.path.join(destination,output_name+".json"), "w") as text_file:
        text_file.write("");
        text_file.write(json)




